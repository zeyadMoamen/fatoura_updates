import Vue from "vue";
import VueRouter from "vue-router";
import Dashboard from "../views/Dashboard.vue";
import NewSale from "../views/Sales/Index.vue";
import Login from "../views/Auth/Login.vue";
import CustomerPayment from "../views/Customers/Payment.vue";
import SupplierPayment from "../views/Suppliers/Payment.vue";
// import Customers from "../views/Customers/Index.vue";
import Products from "../views/Products/Index.vue";
import ReturnSale from "../views/Sales/Return.vue";
import NewPurchase from "../views/Purchases/Index.vue";
import ReturnPurchase from "../views/Purchases/Return.vue";
import Home from "../views/Home"

Vue.use(VueRouter);

const routes = [{
    path: "/",
    name: "Home",
    meta: {
      requiresAuth: true
  },
    component: Home,
  },
  {
    path: "/dashboard",
    name: "Dashboard",
    component: Dashboard,
  },
  {
    path: "/login",
    name: "Login",
    component: Login,
  },
  {
    path: "/customers/payment",
    name: "CustomerPayment",
    component: CustomerPayment,
    meta: {
      requiresAuth: true
  },
  },
  {
    path: "/suppliers/payment",
    name: "SupplierPayment",
    component: SupplierPayment,
    meta: {
      requiresAuth: true
  },
  },
//   {
//     path: '/customers',
//     component: Customers,
//     meta: {
//         requiresAuth: true
//     },
//     children: [{
//             path: '/',
//             component: Customers
//         },
//         {
//             path: 'payment',
//             component: CustomerPayment
//         },

//     ]
// },
  {
    path: "/sales",
    name: "NewSale",
    component: NewSale,
    meta: {
      requiresAuth: true
  },
  },
  {
    path: "/sales/return",
    name: "ReturnSale",
    component: ReturnSale,
    meta: {
      requiresAuth: true
  },
  },
  {
    path: "/purchases",
    name: "NewPurchase",
    component: NewPurchase,
    meta: {
      requiresAuth: true
  },
  },
  {
    path: "/purchases/return",
    name: "ReturnPurchase",
    component: ReturnPurchase,
    meta: {
      requiresAuth: true
  },
 
  },
  {
    path: "/products",
    name: "Products",
    component: Products,
    meta: {
      requiresAuth: true
  },
  },


];

const router = new VueRouter({
  mode: process.env.IS_ELECTRON ? 'hash' : 'history',
  base: process.env.BASE_URL,
  routes,
});

export default router;