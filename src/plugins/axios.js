import Vue from 'vue'
import axios from 'axios'

const axiosApi = axios.create({
    // baseURL: `http://172.104.208.167:2028/api/`,
    baseURL: `http://127.0.0.1:8000/api/`,
    headers: {
        'Content-Type':`application/json`,
         Authorization: `Bearer ${window.localStorage.getItem('token')}` 
    }
})
Vue.use(axios)
export default axiosApi;
