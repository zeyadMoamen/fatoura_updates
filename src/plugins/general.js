import axiosApi from "@/plugins/axios.js";

export function initialize(store, router) {
    router.beforeEach((to, from, next) => {
        // if (
        //     store.getters.currentUser
        // ) {

        //     console.log(store.getters.currentUser.id);
        // } else {
        //     console.log("no user login")
        // }
        let valid=true;
  //       let valid=false;

  //       if (
  //         localStorage.getItem("laravel_app_session") &&
  //         localStorage.getItem("session")
  //       ) {
    
  //         if(localStorage.getItem("laravel_app_session") =="eyJpdiI6Imp4RzYwN0Q5dmxQd3hGSStsML4k13aDS6IMoOjgYuyZY7y0AbUnN2bYUnU/ms9uZmNA=HAMZAL4k13aDS6IMoOjgYuyZY7y0AbUnN2bYUnU/ms9uZmNA=MONGZGJjYzhiNjA4MTJjOThmYjkwNjhjOTc5NmFkZjQif"){

  //     valid=true;
  //         }else{

  //         }

  // }

  console.log('================== start router auth =======================')

       
                const requiresAuth = to.matched.some(record => record.meta.requiresAuth);
                const currentUser = localStorage.getItem('token');
               
                // const userInvenories = store.getters.inventories;
                // console.log("currentUser===> ", currentUser);
        
                
                if (requiresAuth && currentUser == null) {
                    
                    if(to.path == '/login'){
                        next();
                      }else{
    
                        next('/login');
                      }
                } else if (to.path == '/login' && currentUser != null) {
                    // console.log("home")
        
                      
                    next('/');
                } else if(!valid && to.path == '/login' )  {
                    if(to.path == '/login'){
                        next();
                      }else{
    
                        next('/login');
                      }
                } else if(valid)  {
                  next();
                }else{
                  if(to.path == '/forbidden-device'){
                    next();
                  }else{

                    next('/forbidden-device')
                  }

                }      
      
    });

    //! check
    axiosApi.interceptors.response.use((response) => {
        return response;
    }, (error) => {
        if (!error.response) {
            // alert('NETWORK ERROR')
        } else {
            const code = error.response.status
            // const response = error.response.data
            const originalRequest = error.config;

            if (code === 401 && !originalRequest._retry) {
                originalRequest._retry = true

                // store.commit('logout');
                router.push('/login');
            }

            return Promise.reject(error)
        }
    });

  
}

