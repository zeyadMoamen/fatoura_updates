import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import Vuesax from "vuesax";
import "vuesax/dist/vuesax.css";

import "@/plugins/axios.js"
import {
  initialize
} from '@/plugins/general.js';
// import './plugins/axios.js';
import ElementUI from 'element-ui'
import locale from 'element-ui/lib/locale/lang/en'
import 'element-ui/lib/theme-chalk/index.css';
import "popper.js";
import "@/fontawesome-free-5.9.0-web/css/all.css";
Vue.config.productionTip = false;


Vue.use(ElementUI, {
  locale
})
Vue.use(Vuesax);
initialize(store, router);

import AOS from 'aos';
import 'aos/dist/aos.css';
AOS.init();
new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount("#app");